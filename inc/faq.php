<?php
function create_post_type_faq() {

    register_post_type('faq',
        array(
            'labels' => array(
                'name' =>  'FAQ',
                'singular_name' => 'faq',
                'add_new' => 'Добавить',
                'add_new_item' => 'Добавить'
            ),
            'public' => true,
            'menu_position' => 5,
            'has_archive' => true,

            /*	'rewrite' => array(
            'slug' => 'faq'
            ),*/

            'capabilities' => array(),
            'supports' => array(
                'title',
                'editor',
                //'thumbnail',
                'excerpt'

            ),

        ));
}

add_action( 'init', 'create_post_type_faq' );

function create_taxonomy_faq_category() {
    register_taxonomy(
        'faq_category',
        array('faq'),
        array(
            'labels' => array(
                'name' =>  'Категория',
                'singular_name' => 'Категория',
                'add_new' => 'Добавить',
                'add_new_item' => 'Добавить'
            ),
            'hierarchical' => true,
            'show_ui' => true,
        )
    );

}
add_action('init', 'create_taxonomy_faq_category');

?>