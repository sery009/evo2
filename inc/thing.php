<?php
function create_post_type_thing() {

    register_post_type('thing',
        array(
            'labels' => array(
                'name' =>  'Предметы',
                'singular_name' => 'Предмет',
                'add_new' => 'Добавить',
                'add_new_item' => 'Добавить'
            ),
            'public' => true,
            'menu_position' => 5,
            'has_archive' => true,

            /*	'rewrite' => array(
            'slug' => 'thing'
            ),*/

            'capabilities' => array(),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                //'excerpt'

            ),

        ));
}

add_action( 'init', 'create_post_type_thing' );


?>