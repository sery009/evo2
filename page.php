<?php get_header();?>

    <main class="c-main">
        <section class="c-block-type5 c-block-type5-padding">
            <div class="container">
                <div class="c-block-type5-left">
                    <h2 class="c-main-title-type1"><?php the_title();?></h2>
                    <div class="c-block-type5-left-text">
                        <?php the_content();?>
                    </div>
                </div>
                <div class="c-block-type5-right">
                    <?php get_template_part('parts/calc');?>
                </div>
            </div>
        </section>
    </main>

<?php get_footer();?>