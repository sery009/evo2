<footer class="c-footer">
    <div class="container">
        <div class="c-footer-content">
            <div class="c-footer-content-item">
                <div class="c-footer-content-item-title">Эводеньги</div>
                <?php
                wp_reset_query();
                wp_nav_menu( array(
                    'theme_location'  => 'bottom-menu1',
                    'container'       => false,
                    'menu_class'      => 'c-footer-content-item-list c-unlist',

                ) );
                ?>

            </div>
            <div class="c-footer-content-item">
                <div class="c-footer-content-item-title">Сайт</div>
                <?php
                wp_reset_query();
                wp_nav_menu( array(
                    'theme_location'  => 'bottom-menu2',
                    'container'       => false,
                    'menu_class'      => 'c-footer-content-item-list c-unlist',

                ) );
                ?>
            </div>
            <div class="c-footer-content-item">
                <div class="c-footer-content-item-title">Защита данных</div>
                <?php
                wp_reset_query();
                wp_nav_menu( array(
                    'theme_location'  => 'bottom-menu3',
                    'container'       => false,
                    'menu_class'      => 'c-footer-content-item-list c-unlist',

                ) );
                ?>
            </div>
            <div class="c-footer-content-item">
                <div class="c-footer-content-item-title">Контакты</div>
                <div class="c-footer-phone"><a href="tel:88005511955">8 800 55-119-55</a>
                    <div class="c-footer-phone-descr">Бесплатно. Круглосуточно</div>
                </div><a class="c-btn c-btn-type1 js-popup" href="#popup-default">Заказать звонок</a>
            </div>
        </div>
        <div class="c-footer-bottom">
          
           <div class="c-footer-copyright"> Общие вопросы: info@evodengi.ru <br/>
420059, Республика Татарстан г. Казань улица Ботаническая 10а - 1Б </div>
  <div class="c-footer-copyright"> ООО "МКК Эводеньги". Все права защищены. 2018</div>
<!--<a class="c-developer-link" href="http://aims.pro/" target="_blank">made in</a>-->
        </div>
    </div>
</footer>
</div>
<?php get_template_part('parts/hidden');?>
<?php wp_footer();?>
<script src="<?php echo THEME_JS;?>/libs.min.js" defer></script>
<!--[if lt IE 10]-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js" defer></script>
		<!--[endif]-->
		<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/js/jquery.suggestions.min.js" defer></script>
<script src="<?php echo THEME_JS;?>/common.min.js" defer></script>




</body>
</html>
