<?php get_header();?>

    <main class="c-main">
        <section class="c-block-type1 c-block-type1-padding c-block-type1_style1" style="background-image: url(<?php echo THEME_IMG;?>/elements/block1/images/bg.jpg);">
            <div class="container">
                <div class="c-block-type1-left">
                    <h1 class="c-block-type1-title">До <span>30 000 рублей</span><br>на вашу карту за 5 минут</h1>
                    <div class="c-block-type1-text">Займ выплачивается на именные банковские карты <br>(Visa / Master Card / Мир)</div>
                </div>
                <div class="c-block-type1-right">
                    <?php get_template_part('parts/calc');?>
                </div>
            </div>
        </section>
        <section class="c-block-type2 c-block-type2-padding">
            <div class="container">
                <div class="c-block-type2-items">
                    <div class="c-block-type2-item">
                        <div class="c-block-type2-item-ico">
                            <div class="c-ico c-block-type2-ico1"></div>
                        </div>
                        <div class="c-block-type2-item-text">Любым удобным способом <br>за 5 минут</div>
                    </div>
                    <div class="c-block-type2-item">
                        <div class="c-block-type2-item-ico">
                            <div class="c-ico c-block-type2-ico2"></div>
                        </div>
                        <div class="c-block-type2-item-text">Все этапы оформления происходят онлайн</div>
                    </div>
                    <div class="c-block-type2-item">
                        <div class="c-block-type2-item-ico">
                            <div class="c-ico c-block-type2-ico3"></div>
                        </div>
                        <div class="c-block-type2-item-text">Без регистрации</div>
                    </div>
                    <div class="c-block-type2-item">
                        <div class="c-block-type2-item-ico">
                            <div class="c-ico c-block-type2-ico4"></div>
                        </div>
                        <div class="c-block-type2-item-text">Высокий <br>процент одобрения</div>
                    </div>
                </div>
            </div>
        </section>
        <section class="c-block-type3 c-block-type3-padding c-block-type3_style1">
			<div class="container">
				<div class="c-block-type3-form-wrap">
					<h2 class="c-main-title-type1 c-main-title-type1_centred">Я хочу получить</h2>
					<form id="secondForm">
						<div class="c-block-type3-form-title">Сумма займа</div>
						<div class="c-block-type3-form-item">
							<input type="text" name="sum2" value="" data-validate="true"/>
							<input type="text" name="sum-range2" id="sum-range2" value="">
						</div>
						<div class="c-block-type3-form-item">
							<input type="text" name="term2" value="" data-validate="true"/>
							<input type="text" name="term-range2" id="term-range2" value="">
						</div>
						<div class="c-block-type3-form-item">
							<input type="tel" name="phone2" placeholder="+7 (900) 000-0000" value="" data-validate="true"/>
						</div>
						<div class="c-block-type3-form-btn-wrap">
							<button class="c-btn c-btn-type2" type="submit">Получить деньги</button>
							<button class="c-btn c-btn-type2 js-second-form-step2-btn" type="button">Получить деньги</button>
							<button class="c-btn c-btn-type2 js-second-form-step3-btn" type="button">Получить деньги</button>
						</div>
					</form>
				</div>
			</div>
		</section>
        <section class="c-block-type4 c-block-type4-padding">
            <div class="container">
                <h2 class="c-main-title-type1 c-main-title-type1_centred">Как работает сервис</h2>
                <div class="c-main-title-type1-descr c-main-title-type1-descr_centred">Просто. Быстро. Удобно</div>
                <div class="c-block-type4-items">
                    <div class="c-block-type4-item">
                        <div class="c-block-type4-item-ico">
                            <div class="c-ico c-block-type4-ico1"></div>
                        </div>
                        <div class="c-block-type4-item-title">Оформление  заявки онлайн</div>
                        <div class="c-block-type4-item-text">Заполняете данные <br>о сумме и срокам займа</div>
                    </div>
                    <div class="c-block-type4-item">
                        <div class="c-block-type4-item-ico">
                            <div class="c-ico c-block-type4-ico2"></div>
                        </div>
                        <div class="c-block-type4-item-title">Подтверждение</div>
                        <div class="c-block-type4-item-text">После оформления заявки <br>с Вами связывается менеджер <br>для подтверждения данных</div>
                    </div>
                    <div class="c-block-type4-item">
                        <div class="c-block-type4-item-ico">
                            <div class="c-ico c-block-type4-ico3"></div>
                        </div>
                        <div class="c-block-type4-item-title">Получение денег</div>
                        <div class="c-block-type4-item-text">Деньги поступают <br>на выбранный вами счет</div>
                    </div>
                </div>
            </div>
        </section>
        <section class="c-block-type1 c-block-type1-padding c-block-type1_style2" style="background-image: url(<?php echo THEME_IMG;?>/elements/block1/images/bg1.jpg);">
            <div class="container">
                <div class="c-block-type1-left">
                    <h1 class="c-block-type1-title">Деньги на вашей карте уже через 5 минут</h1>
                    <div class="c-block-type1-text">Без подтверждения до 30 000 рублей</div><a class="c-btn c-btn-type2" href="/oformit-zajm">Получить деньги</a>
                </div>
                <div class="c-block-type1-right"></div>
            </div>
        </section>
    </main>

<?php get_footer();?>