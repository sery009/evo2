<?php
/*
 * Template Name: FAQ
 */?><?php get_header();?>
<?php wp_reset_query();
$qo=get_queried_object();
?>
<!--main-->
			<main class="c-main">
				<section class="c-block-type17 c-block-type17-padding">
					<div class="container">
                        <div class="c-main-title-type1">Вопрос-ответ</div>
						<div class="c-block-type17-content">
							<div class="c-block-type17-content-left">
								<div class="c-faq-tabs" id="faqTabs"></div>
                                <?php get_template_part('parts/calc');?>
								<?php
								$faq=new WP_Query("post_type=faq");
								if($faq->have_posts()):
								while($faq->have_posts()):
								$faq->the_post();
								?>
								
								<!--<a class="c-block-type17-banner c-block-type17-banner_1" href="<?php the_permalink()?>">
									<div class="c-block-type17-banner-title"><?php the_title();?></div>
									<div class="c-block-type17-banner-descr"><?php the_excerpt()?></div>
									<div class="c-block-type17-banner-link">Подробнее</div>
								</a>-->
								<?php
								endwhile;
								endif;
								?>
								
							</div>
							<div class="c-block-type17-content-right" id="block17top">
								<div class="c-faq-accordion" id="faqAccordion"></div>
							</div>
						</div>
					</div>
				</section>
			</main>
			<!--main end-->
<?php
$result=array();
$terms=get_terms("faq_category","hide_empty=0");
if($terms)
{
	foreach($terms as $term)
	{
		$result['category'][$term->slug]['title']=$term->name;
		$result['category'][$term->slug]['descr']=$term->description;
		$faq=new WP_Query("post_type=faq&posts_per_page=-1&faq_category=".$term->slug);
		$k=0;
		if($faq->have_posts()):
			while($faq->have_posts()):
				$faq->the_post();
				
				$result['category'][$term->slug]["questions"][$k]['question']=get_the_title();
				$result['category'][$term->slug]["questions"][$k]['answer']=get_the_content();
				$k++;
			endwhile;
		endif;
	}
}
$fp = fopen('faq1.json', 'w');
fwrite($fp, json_encode($result));
fclose($fp);
//var_dump($result);
//echo json_encode($result);
//exit();
?>
<?php get_footer();?>