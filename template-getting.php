<?php
/*
  * Template Name: getting
  */
?><?php
get_header();?>
<main class="c-main">
				<section class="c-block-type8 c-block-type8-padding">
					<div class="container">
						<div class="c-block-type8-top">
							<h2 class="c-main-title-type1">Оформление займа</h2>
						</div>
						<div class="c-block-type8-middle">
							<div class="c-block-type8-steps-nav" id="formWithStepsNav">
								<div class="c-block-type8-steps-nav-items">
									<div class="c-block-type8-steps-nav-item active">
										<div class="c-block-type8-steps-nav-item-title">Шаг 1</div>
										<div class="c-block-type8-steps-nav-item-descr">Создание заявки</div>
									</div>
									<div class="c-block-type8-steps-nav-item">
										<div class="c-block-type8-steps-nav-item-title">Шаг 2</div>
										<div class="c-block-type8-steps-nav-item-descr">Данные заемщика</div>
									</div>
									<div class="c-block-type8-steps-nav-item">
										<div class="c-block-type8-steps-nav-item-title">Шаг 3</div>
										<div class="c-block-type8-steps-nav-item-descr">Дополнительные данные</div>
									</div>
									<div class="c-block-type8-steps-nav-item">
										<div class="c-block-type8-steps-nav-item-title">Шаг 4</div>
										<div class="c-block-type8-steps-nav-item-descr">Данные карты заемщика</div>
									</div>
									<div class="c-block-type8-steps-nav-item">
										<div class="c-block-type8-steps-nav-item-title">Шаг 5</div>
										<div class="c-block-type8-steps-nav-item-descr">Социальные сети</div>
									</div>
									<div class="c-block-type8-steps-nav-item">
										<div class="c-block-type8-steps-nav-item-title">Шаг 6</div>
										<div class="c-block-type8-steps-nav-item-descr">Подписание</div>
									</div>
								</div>
							</div>
						</div>
						<div class="c-block-type8-bottom">
							<div class="c-block-type8-bottom-left">
								<div class="c-block-type8-form-wrap" id="formWithSteps">
									<form>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Создание заявки</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Сумма займа</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="sum" value="" data-validate="true"/>
													<input type="text" name="sum-range" id="sum-range" value="">
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Срок займа</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="term" value="" data-validate="true"/>
													<input type="text" name="term-range" id="term-range" value="">
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Номер телефона</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="tel" name="phone" placeholder="+7 (900) 000-0000" value="" data-validate="true"/>
													<div class="c-agreement">
														<div class="c-agreement-inner">
															<input type="checkbox" name="agreement" value="Даю согласие"/>
															<label></label>
															<div class="c-agreement-text">Даю согласие на обработку своих <a class="js-popup" href="#popup-privacy">персональных данных</a></div>
														</div>
													</div>
												</div>
											</div>
											<div class="c-block-type8-form-bottom">
												<button class="c-btn c-btn-type2" type="button" data-step="#popup-code3" disabled="">Далее</button>
											</div>
										</div>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Основные данные</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">ФИО</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="fullName" placeholder="Петрова Ирина Олеговна" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Ваш пол</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select name="select" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">Мужчина</option>
														<option value="Опция 2">Женщина</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Дата рождения</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input class="js-date-picker" type="text" name="birthday" placeholder="ДД.ММ.ГГГГ" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Электронная почта</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="email" placeholder="example@yandex.ru" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-step-title">Паспортные данные</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Паспорт</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select class="js-select-validate" name="selectPassportType" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">Загрузить фотографии паспорта</option>
														<option value="Опция 2">Ввести данные вручную</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-item-checkbox-wrap-all js-passport-data">
												<div class="c-block-type8-form-item-checkbox-wrap">
													<div class="c-block-type8-form-item-checkbox-wrap-hidden">
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-download-element">
																<div class="c-block-type8-form-item-download-element-title">Загрузите первый разворот паспорта (2,3 страницы)</div>
																<div class="c-block-type8-form-item-download-element-descr">Допустимый формат файлов: .pdf, .doc, .docx, .rtf, .jpg, .png, .bmp.<br>Размер загружаемых файлов не должен превышать 25 Мб.</div>
																<div class="c-input-file js-fileInput">
																	<div class="c-input-file-content">
																		<div class="c-input-file-info">
																			<div class="c-input-file-info-inner">
																				<div class="c-input-file-info-left">
																					<div class="c-ico c-ico-file"></div>
																				</div>
																				<div class="c-input-file-info-right">
																					<div class="c-input-file-info-text">Файл загружен</div>
																					<div class="c-input-file-info-remove">Удалить</div>
																				</div>
																			</div>
																		</div>
																		<div class="c-btn c-btn-fileInput">Загрузить файл</div>
																	</div>
																	<input type="file" name="file-pass1" data-validate="true"/>
																</div>
															</div>
														</div>
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-download-element">
																<div class="c-block-type8-form-item-download-element-title">Страница регистрации</div>
																<div class="c-block-type8-form-item-download-element-descr">Допустимый формат файлов: .pdf, .doc, .docx, .rtf, .jpg, .png, .bmp.<br>Размер загружаемых файлов не должен превышать 25 Мб.</div>
																<div class="c-input-file js-fileInput">
																	<div class="c-input-file-content">
																		<div class="c-input-file-info">
																			<div class="c-input-file-info-inner">
																				<div class="c-input-file-info-left">
																					<div class="c-ico c-ico-file"></div>
																				</div>
																				<div class="c-input-file-info-right">
																					<div class="c-input-file-info-text">Файл загружен</div>
																					<div class="c-input-file-info-remove">Удалить</div>
																				</div>
																			</div>
																		</div>
																		<div class="c-btn c-btn-fileInput">Загрузить файл</div>
																	</div>
																	<input type="file" name="file-pass2" data-validate="true"/>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="c-block-type8-form-item-checkbox-wrap">
													<div class="c-block-type8-form-item-checkbox-wrap-hidden">
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-left">
																<div class="c-block-type8-form-item-title">Серия и номер</div>
															</div>
															<div class="c-block-type8-form-item-right">
																<input type="text" name="passportNumber" placeholder="3409 345698" value=""/>
															</div>
														</div>
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-left">
																<div class="c-block-type8-form-item-title">Дата выдачи</div>
															</div>
															<div class="c-block-type8-form-item-right">
																<input class="js-date-picker" type="text" name="passportDate" placeholder="ДД.ММ.ГГГГ" value=""/>
															</div>
														</div>
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-left">
																<div class="c-block-type8-form-item-title">Город рождения</div>
															</div>
															<div class="c-block-type8-form-item-right">
																<input type="text" name="passportCity" placeholder="Москва" value=""/>
															</div>
														</div>
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-left">
																<div class="c-block-type8-form-item-title">Код подразделения</div>
															</div>
															<div class="c-block-type8-form-item-right">
																<input type="text" name="passportCode" placeholder="321-112" value=""/>
															</div>
														</div>
														<div class="c-block-type8-form-item">
															<div class="c-block-type8-form-item-left">
																<div class="c-block-type8-form-item-title">Кем выдан паспорт</div>
															</div>
															<div class="c-block-type8-form-item-right">
																<input type="text" name="passportFrom" placeholder="УФМС г. Москва" value=""/>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">СНИЛС</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="passportSnils" placeholder="123-456-789-01" value=""/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">ИНН</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="inn" placeholder="123412341234" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-step-title">Адрес регистрации</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-right">
													<input type="text" name="regAddressFull" placeholder="Москва, Московская 45/1, 139 " value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-step-title">Адрес проживания</div>
											<div class="c-block-type8-form-item c-block-type8-form-item_style2">
												<input type="checkbox" name="checkbox-addr" value="checkbox1-value" id="checkbox1"/>
												<label for="checkbox1">Совпадает с адресом регистрации</label>
											</div>
											<div class="c-block-type8-form-item-address">
												<div class="c-block-type8-form-item">
													<div class="c-block-type8-form-item-right">
														<input type="text" name="resAddressFull" placeholder="Москва, Московская 45/1, 139 " value="" data-validate="true"/>
													</div>
												</div>
											</div>
											<div class="c-block-type8-form-bottom">
												<button class="c-btn c-btn-type2" type="button" data-step="3">Далее</button>
											</div>
										</div>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Контакты близкого лица</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">ФИО</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="closePersonFullName" placeholder="Иванов Иван Иванович" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Телефон</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="tel" name="closePersonPhone" placeholder="+7 (900) 000-0000" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-step-title">Дополнительная информация</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Образование</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select name="select" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">Среднее/специальное</option>
														<option value="Опция 2">Неполное высшее</option>
														<option value="Опция 3">Высшее</option>
														<option value="Опция 4">Ученая степень</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Семейное положение</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select name="select" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">В браке</option>
														<option value="Опция 2">Не женат/не замужем</option>
														<option value="Опция 3">Разведен(а)</option>
														<option value="Опция 4">Вдова/вдовец</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Есть ли дети</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select name="select" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">Нет</option>
														<option value="Опция 2">1</option>
														<option value="Опция 3">2</option>
														<option value="Опция 4">3</option>
														<option value="Опция 5">4</option>
														<option value="Опция 6">5+</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Наличие судимости</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select name="select" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">Да</option>
														<option value="Опция 2">Нет</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-step-title">Информация о трудоустройстве</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Организация или ИНН</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="workCompany" placeholder="ООО &quot;Красный Октябрь&quot; или 7707083893" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Должность</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="workPosition" placeholder="Директор" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Тел. работодателя</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="tel" name="workPhone" placeholder="+7 (900) 000-0000" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Стаж</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<select name="select" data-placeholder="Выберите из списка">
														<option value=""></option>
														<option value="Опция 1">До 3 месяцев</option>
														<option value="Опция 2">3 - 6 месяцев</option>
														<option value="Опция 3">6 - 12 месяцев</option>
														<option value="Опция 4">Более года</option>
													</select>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Доход в месяц, руб.</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="workMonthIncome" placeholder="100000" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Дата зарплаты</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input class="js-date-picker" type="text" name="incomeDate" placeholder="ДД.ММ.ГГГГ" value=""/>
												</div>
											</div>
											<div class="c-block-type8-form-step-title">Адрес работы</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-right">
													<input type="text" name="workAddressFull" placeholder="Москва, Московская 45/1, 139 " value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-bottom">
												<button class="c-btn c-btn-type2" type="button" data-step="4">Далее</button>
											</div>
										</div>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Данные карты</div>
											<div class="c-block-type8-form-bottom-descr">Пожалуйста, укажите реквизиты карты, на которую вы хотите получить денежные средства</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Номер карты</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="cardNumber" placeholder="1234 1234 1234 1234" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Владелец карты</div>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="cardHolder" placeholder="ANNA PETROVA" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item c-block-type8-form-item_double-input">
												<div class="c-block-type8-form-item-left">
													<div class="c-block-type8-form-item-title">Действительна до</div>
												</div>
												<div class="c-block-type8-form-item-right c-block-type8-form-item-right_double-input">
													<div>
														<input class="js-date-picker-card" type="text" name="cardValidityDate" placeholder="01 / 22" value="" data-validate="true"/>
													</div>
													<div class="c-block-type8-form-item-right-inner">
														<div class="c-block-type8-form-item-right-inner-left">
															<div class="c-block-type8-form-item-title">CVV</div>
														</div>
														<div class="c-block-type8-form-item-right-inner-right">
															<input type="text" name="cardCvv" placeholder="000" value="" data-validate="true"/>
														</div>
													</div>
												</div>
											</div>
											<div class="c-block-type8-form-bottom">
												<button class="c-btn c-btn-type2" type="button" data-step="5">Далее</button>
											</div>
										</div>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Социальные сети</div>
											<div class="c-block-type8-social-panel">
												<div class="c-block-type8-social-panel-text">Привяжите свои социальные сети к заявке для повышения вероятности одобрения займа.</div>
												<div class="c-block-type8-social-panel-items"><a class="c-block-type8-social-panel-item c-block-type8-social-panel-item_fb" href="#">
														<div class="c-block-type8-social-panel-item-left">
															<div class="c-ico c-ico-fb"></div>
														</div>
														<div class="c-block-type8-social-panel-item-right">
															<div class="c-block-type8-social-panel-item-title">Facebook</div>
														</div></a><a class="c-block-type8-social-panel-item c-block-type8-social-panel-item_insta" href="#">
														<div class="c-block-type8-social-panel-item-left">
															<div class="c-ico c-ico-insta"></div>
														</div>
														<div class="c-block-type8-social-panel-item-right">
															<div class="c-block-type8-social-panel-item-title">Instagram</div>
														</div></a><a class="c-block-type8-social-panel-item c-block-type8-social-panel-item_vk" href="#">
														<div class="c-block-type8-social-panel-item-left">
															<div class="c-ico c-ico-vk"></div>
														</div>
														<div class="c-block-type8-social-panel-item-right">
															<div class="c-block-type8-social-panel-item-title">Вконтакте</div>
														</div></a><a class="c-block-type8-social-panel-item c-block-type8-social-panel-item_tw" href="#">
														<div class="c-block-type8-social-panel-item-left">
															<div class="c-ico c-ico-tw"></div>
														</div>
														<div class="c-block-type8-social-panel-item-right">
															<div class="c-block-type8-social-panel-item-title">Twitter</div>
														</div></a></div>
											</div>
											<div class="c-block-type8-form-bottom c-block-type8-form-bottom_style1">
												<button class="c-btn c-btn-type2" type="button" data-step="6">Продолжить</button>
											</div>
										</div>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Подтверждение</div>
											<div class="c-block-type8-confirm-field">
												<div class="c-block-type8-confirm-field-item">
													<div class="c-block-type8-confirm-field-item-text">Я, <span>Петрова Ирина Олеговна</span></div>
													<div class="c-block-type8-confirm-field-item">
														<div class="c-block-type8-confirm-field-item-text">Хочу оформить займ на сумму <span>23 000 </span>рублей.</div>
													</div>
													<div class="c-block-type8-confirm-field-item">
														<div class="c-block-type8-confirm-field-item-text">Я планирую погасить долг по займу в течении <span>14 </span>дней.</div>
													</div>
													<div class="c-block-type8-confirm-field-item">
														<div class="c-block-type8-confirm-field-item-text">Мне <span>42 </span>года, я - <span>женщина.</span></div>
													</div>
													<div class="c-block-type8-confirm-field-item">
														<div class="c-block-type8-confirm-field-item-text">Живу в городе <span>Петропавловск-Камчатский</span></div>
													</div>
													<div class="c-block-type8-confirm-field-item">
														<div class="c-block-type8-confirm-field-item-text">Мой ежемесячный доход составляет <span>70 000 </span>рублей.</div>
													</div>
												</div>
											</div>
											<div class="c-block-type8-form-bottom"><a class="c-btn c-btn-type2 js-popup" href="#popup-code">Подтвердить</a></div>
										</div>
										<div class="c-block-type8-form-step" id="thanks">
											<div class="c-block-type8-result">
												<div class="c-block-type8-result-title">Ваша заявка рассматривается.</div>
												<div class="c-block-type8-result-text">В течении 5 минут вы получите СМС с решением.<br>Номер заявки <span>№3254</span></div>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="c-block-type8-bottom-right">
								<div id="sticky1">
									<div class="c-block-type8-form-step-title">Информация по займу</div>
									<div class="c-block-type8-bottom-right-items">
										<div class="c-block-type8-bottom-right-item">
											<div class="c-block-type8-bottom-right-item-title">Сумма займа</div>
											<div class="c-block-type8-bottom-right-item-text c-block-type8-bottom-right-item-text_accent js-total-amount"></div>
										</div>
										<div class="c-block-type8-bottom-right-item">
											<div class="c-block-type8-bottom-right-item-title">Дата возврата</div>
											<div class="c-block-type8-bottom-right-item-text c-block-type8-bottom-right-item-text_accent js-total-term"></div>
										</div>
										<div class="c-block-type8-bottom-right-item">
											<div class="c-block-type8-bottom-right-item-title">К возврату</div>
											<div class="c-block-type8-bottom-right-item-text">24 700 руб.</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main><?php get_footer();?>
