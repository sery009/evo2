<?php get_header();?>

    <main class="c-main">
        <div class="c-block-type10">
            <div class="c-block-type10-top">
                <div class="container">
                    <h2 class="c-block-type10-title"><?php
                        wp_reset_query();
                        $qo=get_queried_object();
                        if(is_tax())
                        {
                            echo $qo->name;
                        }
                        else
                        {
                            the_title();
                        }
                        ?></h2>
                    <div class="c-block-type10-search">
                        <form id="search-form" action="<?php bloginfo('url')?>" class="search">
                            <input type="text" name="s" placeholder="Поиск" value="" data-validate=""/>
                        </form>
                    </div>
                </div>
                <div class="container">
                    <div class="c-block-type10-categories">
                        <a class="c-block-type10-categories-item <?php if(!is_category())echo'active';?>" href="<?php bloginfo('url')?>/news">Все новости</a>
                        <?php
                        $terms=get_categories("hide_empty=0");
                        if($terms)
                        {
                            foreach($terms as $term)
                            {
                                ?>
                                <a class="c-block-type10-categories-item <?php if(is_category($term)&&$qo->term_id==$term->term_id)echo'active';?>" href="<?php echo get_term_link($term);?>"><?php echo $term->name?></a>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="c-block-type10-middle">
                <div class="container">
                    <div class="c-block-type10-middle-items append_data">
                        <?php
                        wp_reset_query();
                        $posts=new WP_Query('post_type=post&posts_per_page=12');
                        if($posts->have_posts()):
                            while ($posts->have_posts()):
                                $posts->the_post();
                        get_template_part('parts/article');
                            endwhile;
                        endif;
                        ?>

                    </div>
                </div>
            </div>
            <?php if (  $posts->max_num_pages > 1 ) : ?>
                <script>
                    var ajaxurl = '<?php bloginfo('template_url') ?>/ajax/more.php';
                    var true_posts = '<?php echo serialize($posts->query_vars); ?>';
                    var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                    var max_pages = '<?php echo $posts->max_num_pages; ?>';
                </script>
                <div class="c-block-type10-bottom">
                    <div class="container"><a class="c-block-type10-bottom-link more_posts_link" href="javascript:void(0)">Показать больше статей</a></div>
                </div>
            <?php endif; ?>

        </div>
        <section class="c-block-type3 c-block-type3-padding c-block-type3_style1">
            <div class="container">
                <div class="c-block-type3-form-wrap">
                    <h2 class="c-main-title-type1 c-main-title-type1_centred">Я хочу получить</h2>
                    <form>
                        <div class="c-block-type3-form-title">Сумма займа</div>
                        <div class="c-block-type3-form-item">
                            <input type="text" name="sum2" value="" data-validate="true"/>
                            <input type="text" name="sum-range2" id="sum-range2" value="">
                        </div>
                        <div class="c-block-type3-form-btn-wrap">
                            <button class="c-btn c-btn-type2">Оформить займ</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

<?php get_footer()?>