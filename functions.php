<?php
define('THEME_PATH', get_stylesheet_directory());
define('THEME_ROOT', get_stylesheet_directory_uri());
define('THEME_CSS', THEME_ROOT . '/css');
define('THEME_JS', THEME_ROOT . '/js');
define('THEME_IMG', THEME_ROOT . '/img');
define('THEME_VIDEO', THEME_ROOT . '/movie');


require_once(THEME_PATH . "/inc/faq.php");
add_theme_support('post-thumbnails');

register_nav_menus(array(
    'top-menu' => 'Верхнее меню',
    'bottom-menu1' => 'Нижнее меню1',
    'bottom-menu2' => 'Нижнее меню2',
    'bottom-menu3' => 'Нижнее меню3',
));



function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more($more)
{
return;
}
add_filter('excerpt_more','new_excerpt_more');


function myextensionTinyMCE($init) {
    // Command separated string of extended elements
    $ext = 'span[id|name|class|style]|noindex';

    // Add to extended_valid_elements if it alreay exists
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }

    // Super important: return $init!
    return $init;
}

add_filter('tiny_mce_before_init', 'myextensionTinyMCE' );

function customize_scripts_output(){
    global $wp_scripts;

    if(false != $wp_scripts->queue)
        foreach($wp_scripts->queue as $script)
            if(isset($wp_scripts->registered[$script]))
                $wp_scripts->registered[$script]->deps = array();
}
add_action('wp_enqueue_scripts', 'customize_scripts_output', 101);

function thumb($id,$size)
{
    if(has_post_thumbnail($id))
        return"<img  src='".get_thumb_src($id,$size)."'  alt='".get_the_title()."'/>";
        //return get_the_post_thumbnail($id,$size,array("alt"=>get_the_title($id),"title"=>get_the_title()));
    return"";
}

function get_thumb_src($id,$size)
{
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id), $size );
    $url = $thumb['0'];
    return $url;
}

function getAttachmentSrcById($id,$size){
    $thumb = wp_get_attachment_image_src( $id, $size );
    $url = $thumb['0'];
    return $url;
}

/*add_action( 'admin_menu', 'xelly_remove_menu_items' );

function xelly_remove_menu_items() {
    // тут мы укахываем ярлык пункты который удаляем.
    remove_menu_page('edit-comments.php');
    remove_menu_page('edit.php');
    remove_menu_page('themes.php');
    remove_menu_page('profile.php');
    remove_menu_page('tools.php');
    remove_menu_page('index.php');
    remove_menu_page('upload.php');
}

function loginRedirect( $redirect_to, $request, $user ){
    if( is_array( $user->roles ) ) { // check if user has a role
        return "wp-admin/admin.php?page=acf-options";
    }
}
add_filter("login_redirect", "loginRedirect", 10, 3);*/

function getTermName($post_id)
{
    $terms=get_the_category($post_id);
    if($terms)
    {
        foreach($terms as $term)
        {

            $name=$term->name;
            return $name;
        }
    }
}

function getTermLink($post_id)
{
    $terms=get_the_category($post_id);
    if($terms)
    {
        foreach($terms as $term)
        {
            $link=get_term_link($term);
            return $link;
        }
    }
}



function atg_menu_classes($classes, $item, $args) {
    if($args->theme_location == 'top-menu') {
        $classes[] = 'c-header-nav-item';
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);



add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );
function wpse156165_menu_add_class( $atts, $item, $args ) {
    if($args->theme_location == 'top-menu') {
        $class = 'c-header-nav-item-link'; // or something based on $item
        $atts['class'] = $class;
    }
    return $atts;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}