<div class="hidden">
			<div class="c-popup-default" id="popup-default">
				<div class="c-popup-default-inner">
					<h2 class="c-popup-default-title">Есть вопросы?</h2>
					<div class="c-popup-default-title-descr">Оставьте номер и менеджер перезвонит вам для подробной консультации</div>
					<form>
						<div class="c-popup-default-form-item">
							<input type="text" name="name" placeholder="Ваше имя" value="" data-validate="true"/>
						</div>
						<div class="c-popup-default-form-item">
							<input type="tel" name="phone" placeholder="Номер телефона" value="" data-validate="true"/>
						</div>
						<input type="hidden" name="form_name" value="Обратный звонок"/>
						<div class="c-popup-default-form-btn-wrap">
							<button class="c-btn c-btn-type2">Позвоните мне</button>
						</div>
						<div class="c-agreement">
							<div class="c-agreement-inner">
								<input type="checkbox" name="agreement" value="Даю согласие"/>
								<label></label>
								<div class="c-agreement-text">Даю согласие на обработку своих <a class="js-popup" href="#popup-privacy">персональных данных</a></div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="c-popup-default c-popup-code" id="popup-code">
				<div class="c-popup-default-inner">
					<h2 class="c-popup-default-title">Введите код для завершения</h2>
					<form>
						<div class="c-popup-code-text">На номер <span>8 (987) 29-27-450 </span>был направлен код SMS-сообщением, <br>введите его для подтверждения</div>
						<div class="c-popup-code-form-item">
							<input name="code" placeholder="721 234" value="" data-validate="true" type="text">
						</div>
						<div class="c-popup-code-bottom"><a class="c-popup-code-bottom-link c-popup-code-bottom-link_style1" href="#">Изменить номер</a><br><a class="c-popup-code-bottom-link c-popup-code-bottom-link_style2" href="#">Отправить еще раз</a></div>
						<input name="form_name" value="Стандартная форма заявки" type="hidden">
						<button class="c-btn c-btn-type2" type="button" data-type="thanks">Подтвердить</button>
					</form>
				</div>
			</div>
			<div class="c-popup-default c-popup-code" id="popup-code2">
				<div class="c-popup-default-inner">
					<h2 class="c-popup-default-title">Введите код доступа из SMS</h2>
					<form>
						<div class="c-popup-code-text">Для доступа к информации по займу, необходимо ввести код доступа. <br>Код был направлен на номер телефона, указанный при оформлении договора.</div>
						<div class="c-popup-code-form-item">
							<input type="text" name="code" placeholder="721 234" value="" data-validate="true"/>
						</div>
						<div class="c-popup-code-bottom"><a class="c-popup-code-bottom-link c-popup-code-bottom-link_style2" href="#">Отправить код еще раз</a></div>
						<input type="hidden" name="form_name" value="Стандартная форма заявки"/>
						<button class="c-btn c-btn-type2" type="button" data-type="info">Далее</button>
					</form>
				</div>
			</div>
			<div class="c-popup-default c-popup-code" id="popup-code3">
				<div class="c-popup-default-inner">
					<h2 class="c-popup-default-title">Подтверждение телефона</h2>
					<form>
						<div class="c-popup-code-text">На номер <span class="js-popup-phone1"></span>был направлен код SMS-сообщением, <br>введите его для подтверждения</div>
						<div class="c-popup-code-form-item">
							<input name="code" placeholder="721 234" value="" data-validate="true" type="text">
						</div>
						<div class="c-popup-code-bottom"><a class="c-popup-code-bottom-link c-popup-code-bottom-link_style1" href="#">Изменить номер</a><br><a class="c-popup-code-bottom-link c-popup-code-bottom-link_style2" href="#">Отправить еще раз</a></div>
						<input name="form_name" value="Стандартная форма заявки" type="hidden">
						<button class="c-btn c-btn-type2" type="button" data-type="step2">Подтвердить</button>
					</form>
				</div>
			</div>
			<div class="c-popup-default c-popup-privacy" id="popup-privacy">
				<div class="c-popup-default-inner">
					<h2 class="c-popup-default-title">ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ</h2>
					<div class="c-popup-privacy-content">
						<h3>1.Общие условия</h3>
						<p>1.1. Настоящая Политика определяет порядок обработки и защиты информации о физических лицах (далее – Пользователь), которая может быть получена нашей компанией при использовании Пользователем данного сайта.</p>
						<p>1.2. Целью настоящей Политики является обеспечение надлежащей защиты персональной информации, которую Пользователь предоставляет о себе самостоятельно при использовании данного Сайта для приобретения товаров/услуг, от несанкционированного доступа и разглашения.</p>
						<p>1.3. Отношения, связанные со сбором, хранением, распространением и защитой информации предоставляемой Пользователем, регулируются настоящей Политикой и действующим законодательством Российской Федерации.</p>
						<p>1.4. Оставляя свои данные на Сайте и используя данный Сайт, Пользователь выражает свое полное согласие с условиями настоящей Политики.</p>
						<p>1.5. В случае несогласия Пользователя с условиями настоящей Политики использование данного Сайта должно быть немедленно прекращено.</p>
						<h3>2. Цели сбора, обработки и хранения информации предоставляемой пользователями Сайта</h3>
						<p>2.1. Обработка персональных данных Пользователя осуществляется в соответствии с законодательством Российской Федерации. Наша компания обрабатывает персональные данные Пользователя в целях: - связи с Пользователем, направлении Пользователю транзакционных писем в момент получения заявки с формы на Сайте, направлении Пользователю уведомлений, запросов.</p>
						<p>    h3 3. Условия обработки персональной информации предоставленной Пользователем и ее передачи третьим лицам</p>
						<p>3.1. Наша компания принимает все необходимые меры для защиты персональных данных Пользователя от неправомерного доступа, изменения, раскрытия или уничтожения.</p>
						<p>3.2. Наша компания предоставляет доступ к персональным данным Пользователя только тем работникам, которым эта информация необходима для обеспечения функционирования Сайта и оказания Услуг/продажи товаров Пользователю.</p>
						<p>3.3. Наша компания вправе использовать предоставленную Пользователем информацию, в том числе персональные данные, в целях обеспечения соблюдения требований действующего законодательства Российской Федерации. Раскрытие предоставленной Пользователем информации может быть произведено лишь в соответствии с действующим законодательством Российской Федерации.</p>
						<h3>4. Условия пользования данным Сайтом</h3>
						<p>4.1. Пользователь при пользовании Сайтом, подтверждает, что: - ознакомлен с настоящей Политикой, выражает свое согласие с ней и принимает на себя указанные в ней права и обязанности.</p>
						<p>4.2. Наша компания не проверяет достоверность получаемой (собираемой) информации о Пользователях.</p>
						<h3>5. В рамках настоящей Политики под «персональной информацией Пользователя» понимаются:</h3>
						<p>5.1. Данные предоставленные Пользователем самостоятельно при пользовании данным Сайтом, включая но, не ограничиваясь: имя, фамилия, пол, номер мобильного телефона и/или адрес электронной почты, город проживания, домашний адрес.</p>
					</div>
				</div>
			</div>
		</div>