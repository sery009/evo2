<div class="c-block-type1-form-wrap">
								<form id="mainForm">
									<div class="c-block-type1-form-top">
										<div class="c-block-type1-form-title">Оформите займ</div>
										<div class="c-block-type1-form-descr">Получите деньги уже через 5 минут</div>
										<div class="c-block-type1-form-items">
											<div class="c-block-type1-form-item">
												<div class="c-block-type1-form-item-title">Сумма займа</div>
												<input type="text" name="sum" value="" data-validate="true"/>
												<input type="text" name="sum-range" id="sum-range" value="">
											</div>
											<div class="c-block-type1-form-item">
												<div class="c-block-type1-form-item-title">Срок займа</div>
												<input type="text" name="term" value="" data-validate="true"/>
												<input type="text" name="term-range" id="term-range" value="">
											</div>
											<div class="c-block-type1-form-item js-main-form-phone-field">
												<div class="c-block-type1-form-item-title">Телефон</div>
												<input type="tel" name="phone" placeholder="+7 (900) 000-0000" value="" data-validate="true"/>
											</div>
										</div>
									</div>
									<div class="c-block-type1-form-bottom">
										<div class="c-block-type1-form-bottom-btn-wrap">
											<button class="c-btn c-btn-type2" type="submit">Получить деньги</button>
											<button class="c-btn c-btn-type2 js-main-form-step2-btn" type="button">Оформить займ</button>
										</div>
									</div>
								</form>
							</div>