<a class="c-block-type10-middle-item" href="<?php the_permalink();?>">
    <div class="c-block-type10-middle-item-img"><?php echo thumb(get_the_ID(),"full")?></div>
    <div class="c-block-type10-middle-item-content">
        <div class="c-block-type10-middle-item-title"><?php the_title();?></div>
        <div class="c-block-type10-middle-item-text"><?php echo get_the_excerpt();?></div>
    </div>
    <div class="c-block-type10-middle-info">
        <div class="c-block-type10-middle-info-left">
            <div class="c-block-type10-middle-info-time"><?php the_time('d.m.Y');?></div>
        </div>
        <div class="c-block-type10-middle-info-right">
            <div class="c-block-type10-middle-info-items">
                <div class="c-block-type10-middle-info-item c-block-type10-middle-info-item_1"><?php echo get_post_meta(get_the_ID(),"kub_views",true);?></div>
                <!--<div class="c-block-type10-middle-info-item c-block-type10-middle-info-item_2">12</div>-->
            </div>
        </div>
    </div>
</a>