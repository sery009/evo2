<?php header("Content-Type: text/html; charset=utf-8");
//echo getPerPage();
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="shortcut icon" href="<?php echo THEME_IMG;?>/favicon/favicon.png" type="image/x-icon">
    <!--normalize.css v5.0.0-->
    <style>html{font-family:sans-serif;line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,footer,header,nav,section{display:block}h1{font-size:2em;margin:.67em 0}figcaption,figure,main{display:block}figure{margin:1em 40px}hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}a:active,a:hover{outline-width:0}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:inherit}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}audio,video{display:inline-block}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:sans-serif;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=reset],[type=submit],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{display:inline-block;vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details,menu{display:block}summary{display:list-item}canvas{display:inline-block}template{display:none}[hidden]{display:none}html{-webkit-box-sizing:border-box;box-sizing:border-box}*,::after,::before{-webkit-box-sizing:inherit;box-sizing:inherit}@-ms-viewport{width:device-width}html{-ms-overflow-style:scrollbar;-webkit-tap-highlight-color:transparent}body{font-family:-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;font-size:1rem;font-weight:400;line-height:1.5;color:#292b2c;background-color:#fff}[tabindex="-1"]:focus{outline:0!important}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[data-original-title],abbr[title]{cursor:help}address{margin-bottom:1rem;font-style:normal;line-height:inherit}dl,ol,ul{margin-top:0;margin-bottom:1rem}ol ol,ol ul,ul ol,ul ul{margin-bottom:0}dt{font-weight:700}dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}a{color:#0275d8;text-decoration:none}a:focus,a:hover{color:#014c8c;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus,a:not([href]):not([tabindex]):hover{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle}[role=button]{cursor:pointer}[role=button],a,area,button,input,label,select,summary,textarea{-ms-touch-action:manipulation;touch-action:manipulation}table{border-collapse:collapse;background-color:transparent}caption{padding-top:.75rem;padding-bottom:.75rem;color:#636c72;text-align:left;caption-side:bottom}th{text-align:left}label{display:inline-block;margin-bottom:.5rem}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}button,input,select,textarea{line-height:inherit}input[type=checkbox]:disabled,input[type=radio]:disabled{cursor:not-allowed}input[type=date],input[type=time],input[type=datetime-local],input[type=month]{-webkit-appearance:listbox}textarea{resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;line-height:inherit}input[type=search]{-webkit-appearance:none}output{display:inline-block}[hidden]{display:none!important}</style>
    <!--preloader-->
    <style>body{opacity:0;overflow-x:hidden;}html{background-color:#fff;}</style>
    <link rel="stylesheet" href="<?php echo THEME_CSS;?>/libs.min.css">
    <link rel="stylesheet" href="<?php echo THEME_CSS;?>/main.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/css/suggestions.min.css">
    <?php
    wp_head();
    ?>
</head>
<body>
<div class="c-wrapper">
    <div class="c-fixed-aside">
        <div class="c-fixed-aside-top">
            <div class="c-fixed-aside-top-items">
                <div class="c-fixed-aside-top-item"><a class="c-btn c-btn-type3" href="#"><span>Перезвоните мне</span></a></div>
                <div class="c-fixed-aside-top-item"><a class="c-btn c-btn-type4" href="#"><span>Оформить займ</span></a></div>
            </div>
        </div>

        <div class="c-fixed-aside-bottom">
            <div class="c-fixed-aside-bottom-close-btn"></div>
            <div class="c-fixed-aside-bottom-item">
                <div class="c-fixed-aside-bottom-item-left">
                    <div class="c-ico c-ico-in"></div>
                </div>
                <div class="c-fixed-aside-bottom-item-right">
                    <div class="c-fixed-aside-bottom-item-title">Сегодня выдано:</div>
                    <div class="c-fixed-aside-bottom-item-text c-fixed-aside-bottom-item-text_style1">72 500 руб.</div>
                </div>
            </div>
            <div class="c-fixed-aside-bottom-item">
                <div class="c-fixed-aside-bottom-item-left">
                    <div class="c-ico c-ico-out"></div>
                </div>
                <div class="c-fixed-aside-bottom-item-right">
                    <div class="c-fixed-aside-bottom-item-title">Сегодня вернули:</div>
                    <div class="c-fixed-aside-bottom-item-text c-fixed-aside-bottom-item-text_style2">54 350 руб.</div>
                </div>
            </div>
        </div>
    </div>
    <header class="c-header c-header-padding">
        <div class="container">
            <div class="c-header-left"><a class="c-header-logo" href="<?php bloginfo('url')?>"><svg id="_1" data-name="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 199.95 31.16"><defs><style>.cls-1{fill:#1b1b1b;}.cls-1,.cls-2{fill-rule:evenodd;}.cls-2{fill:#00c112;}</style></defs><title>эво деньги логотип ai</title><path class="cls-1" d="M334.4,69.34h-0.53V66.47l-1.2,2.86h-0.45L331,66.47v2.86h-0.53v-3.6h0.77l1.18,3,1.19-3h0.77v3.6h0Zm2.41-3.7a1.88,1.88,0,0,1,1.88,1.88,1.89,1.89,0,0,1-.54,1.35,1.75,1.75,0,0,1-1.32.56,1.78,1.78,0,0,1-1.32-.55,1.83,1.83,0,0,1-.54-1.33,1.89,1.89,0,0,1,.54-1.34,1.73,1.73,0,0,1,1.31-.57h0Zm1.33,1.91a1.39,1.39,0,0,0-.36-1,1.24,1.24,0,0,0-1-.39,1.23,1.23,0,0,0-.95.39,1.51,1.51,0,0,0,0,2,1.22,1.22,0,0,0,.94.39,1.25,1.25,0,0,0,1-.39,1.36,1.36,0,0,0,.36-1h0Zm5,1.8h-0.53V66.47l-1.2,2.86H341l-1.21-2.86v2.86h-0.53v-3.6H340l1.18,3,1.19-3h0.77v3.6h0Zm4.27-1.62h-3.13a1.27,1.27,0,0,0,.39.88,1.28,1.28,0,0,0,.9.32,1.21,1.21,0,0,0,1.18-.61h0.61a1.9,1.9,0,0,1-1.78,1.13,1.79,1.79,0,0,1-1.34-.54,1.87,1.87,0,0,1-.53-1.36,1.84,1.84,0,0,1,.54-1.34,1.8,1.8,0,0,1,1.34-.54,1.65,1.65,0,0,1,1.36.62,2.25,2.25,0,0,1,.46,1.46h0Zm-3.13-.48h2.56a1.12,1.12,0,0,0-1.28-1.07,1.24,1.24,0,0,0-.84.29,1.34,1.34,0,0,0-.44.78h0Zm6.6,2.1h-0.54V67.73h-1.82v1.61H348v-3.6h0.54v1.51h1.82V65.74h0.54v3.6h0Zm0.43-3.6h2.39v0.49h-0.93v3.11h-0.54V66.23h-0.93V65.74h0Zm6.35,3.6h-0.54V68.8a1.64,1.64,0,0,1-1.31.62,1.78,1.78,0,0,1-1.33-.54,2,2,0,0,1,0-2.68,1.78,1.78,0,0,1,1.33-.55,1.56,1.56,0,0,1,1.31.65V65.74h0.54v3.6h0Zm-1.83-3.18a1.22,1.22,0,0,0-.93.41,1.43,1.43,0,0,0,0,1.94,1.22,1.22,0,0,0,.94.41,1.19,1.19,0,0,0,.91-0.42,1.38,1.38,0,0,0,.38-1,1.4,1.4,0,0,0-.37-1,1.18,1.18,0,0,0-.92-0.42h0Zm5.42,3.18h-0.59l-1-2.77h0l-1,2.76H358l1.39-3.6h0.46l1.39,3.6h0Zm0.36,0v-3.6h0.53v1.45h0.71a1,1,0,0,1,1.23,1.07,1,1,0,0,1-.36.81,1.35,1.35,0,0,1-.86.27h-1.24Zm0.53-1.66v1.16h0.67a0.59,0.59,0,1,0,0-1.16h-0.68Zm5.39,1.66H367V67.73h-1.82v1.61h-0.54v-3.6h0.54v1.51H367V65.74h0.54v3.6h0Zm4.08,0h-0.55v-3.6h0.55v3.6h0Zm-3.31,0v-3.6h0.53v1.44h0.72a1.16,1.16,0,0,1,.83.28,1,1,0,0,1,.3.8,1,1,0,0,1-.33.79,1.24,1.24,0,0,1-.84.28h-1.2Zm0.53-1.66v1.16h0.62a0.59,0.59,0,1,0,0-1.16h-0.63Zm7.06,0h-3.13a1.27,1.27,0,0,0,.39.88,1.28,1.28,0,0,0,.9.32,1.21,1.21,0,0,0,1.18-.61h0.61a1.9,1.9,0,0,1-1.78,1.13,1.79,1.79,0,0,1-1.34-.54,1.87,1.87,0,0,1-.53-1.36,1.84,1.84,0,0,1,.54-1.34,1.8,1.8,0,0,1,1.34-.54,1.65,1.65,0,0,1,1.36.62,2.25,2.25,0,0,1,.46,1.46h0Zm-3.13-.48h2.56A1.12,1.12,0,0,0,374,66.16a1.24,1.24,0,0,0-.84.29,1.34,1.34,0,0,0-.44.78h0Zm5.41,1h0.57a0.6,0.6,0,0,0,.25.51,0.89,0.89,0,0,0,.56.17,1.17,1.17,0,0,0,.57-0.13,0.5,0.5,0,0,0,.29-0.46,0.54,0.54,0,0,0-.39-0.58,3.28,3.28,0,0,0-.77-0.06V67.25a2.44,2.44,0,0,0,.69-0.07,0.49,0.49,0,0,0,.38-0.5,0.47,0.47,0,0,0-.22-0.42,0.84,0.84,0,0,0-.48-0.13,1.06,1.06,0,0,0-.53.12,0.5,0.5,0,0,0-.25.45h-0.55a0.93,0.93,0,0,1,.42-0.82,1.59,1.59,0,0,1,.92-0.25,1.39,1.39,0,0,1,.86.26,0.93,0.93,0,0,1,.37.79,0.77,0.77,0,0,1-.62.75,0.92,0.92,0,0,1,.72.92,0.94,0.94,0,0,1-.46.81,1.64,1.64,0,0,1-.93.27,1.54,1.54,0,0,1-1-.29,1,1,0,0,1-.43-0.88h0Zm6.9,1.09h-0.54V68.8a1.64,1.64,0,0,1-1.31.62,1.78,1.78,0,0,1-1.33-.54,2,2,0,0,1,0-2.68,1.78,1.78,0,0,1,1.33-.55,1.56,1.56,0,0,1,1.31.65V65.74h0.54v3.6h0Zm-1.83-3.18a1.22,1.22,0,0,0-.93.41,1.43,1.43,0,0,0,0,1.94,1.22,1.22,0,0,0,.94.41,1.19,1.19,0,0,0,.91-0.42,1.38,1.38,0,0,0,.38-1,1.4,1.4,0,0,0-.37-1,1.18,1.18,0,0,0-.92-0.42h0Zm5.07-1.66a0.92,0.92,0,0,1-1,.88,0.93,0.93,0,0,1-.66-0.24,1,1,0,0,1-.3-0.63h0.46a0.43,0.43,0,0,0,.15.34,0.51,0.51,0,0,0,.35.13,0.51,0.51,0,0,0,.35-0.14,0.43,0.43,0,0,0,.15-0.33h0.46Zm0.56,4.84h-0.54V66.52l-2,2.81h-0.55v-3.6h0.53v2.82l2-2.82h0.56v3.6h0Zm4.66,0H393V66.47l-1.2,2.86h-0.45l-1.21-2.86v2.86h-0.53v-3.6h0.77l1.18,3,1.19-3h0.77v3.6h0Zm4.07,0h-0.55v-3.6h0.55v3.6h0Zm-3.31,0v-3.6h0.53v1.44h0.72a1.16,1.16,0,0,1,.83.28,1,1,0,0,1,.3.8,1,1,0,0,1-.33.79,1.24,1.24,0,0,1-.84.28h-1.2Zm0.53-1.66v1.16h0.62a0.59,0.59,0,1,0,0-1.16h-0.63Z" transform="translate(-197.66 -38.27)"></path><path class="cls-1" d="M320.84,64.48H318.5V62.14H307.28v2.34h-2.34V59.86h1.5l5.23-14.4h2.41l5.27,14.4h1.5v4.62h0Zm-4.09-4.62L312.94,49l-0.11.06L309,59.86h7.71ZM339,54.63h-14.5a5.87,5.87,0,0,0,1.79,4.06,6,6,0,0,0,4.19,1.46q4,0,5.46-2.83h2.83q-2.73,5.23-8.26,5.23a8.29,8.29,0,0,1-6.21-2.5,8.67,8.67,0,0,1-2.44-6.31,8.51,8.51,0,0,1,2.49-6.22A8.35,8.35,0,0,1,330.52,45a7.67,7.67,0,0,1,6.31,2.86A10.44,10.44,0,0,1,339,54.63h0Zm-14.5-2.21h11.86q-0.55-5-5.95-5a5.75,5.75,0,0,0-3.89,1.35,6.23,6.23,0,0,0-2,3.62h0Zm30.59,9.72h-2.5V54.7h-8.42v7.44h-2.5V45.46h2.5v7h8.42v-7h2.5V62.14h0Zm3.51,0V45.46H361v6.73h3.29q4,0,5.29,2.76a5.22,5.22,0,0,1,.42,2.21,4.49,4.49,0,0,1-1.69,3.74,6.25,6.25,0,0,1-4,1.24h-5.75ZM361,54.47v5.36h3.1q3.4,0,3.4-2.67t-3.36-2.7H361Zm20.35-6.66h-6.24V62.14h-2.5V45.46h8.74V47.8h0Zm16.25,14.33h-2.5v-13l-9.07,13h-2.6V45.46h2.5v13l9.08-13h2.58V62.14Z" transform="translate(-197.66 -38.27)"></path><path class="cls-2" d="M284.11,66.51l10.66-19.18A19.68,19.68,0,0,1,297.92,58h0l-13.56,11.3H271.94L258.38,58h0a19.68,19.68,0,0,1,3.15-10.71l10.66,19.18h11.93Zm-10.36-2.82h1.06l-4.34-23.87a19.82,19.82,0,0,0-7.13,5.13l10.41,18.75h0Zm5.33,0,4.13-24.76a19.89,19.89,0,0,0-10.14,0l4.13,24.76h1.88Zm3.45,0L293,44.94a19.82,19.82,0,0,0-7.13-5.13l-4.34,23.87h1.06Z" transform="translate(-197.66 -38.27)"></path><path class="cls-2" d="M214.89,53.72A9,9,0,0,1,200.72,61a8.09,8.09,0,0,1-3.06-4.55H202q0.78,2.37,4,2.37a4.7,4.7,0,0,0,4.58-3.54h-5.88V52.09h5.88a4,4,0,0,0-1.51-2.54,4.56,4.56,0,0,0-2.88-.94q-3.35,0-4.16,2.28h-4.36a7.82,7.82,0,0,1,3.09-4.52A8.9,8.9,0,0,1,206,44.81a8.56,8.56,0,0,1,6.37,2.5,8.73,8.73,0,0,1,2.47,6.4h0ZM231.3,57.3q0,4.84-6.76,4.84h-7V45.3h7q6,0,6,4.52a3.6,3.6,0,0,1-2.08,3.38v0a4.16,4.16,0,0,1,2.89,4.06h0Zm-5-7.15q0-1.69-2.28-1.69h-2.37v3.64H224q2.37,0,2.37-2h0Zm0.49,7q0-2.08-2.63-2.08h-2.5v3.87h2.7q2.44,0,2.44-1.79h0Zm10.73-3.38a5.14,5.14,0,0,0,1.28,3.59,4.94,4.94,0,0,0,6.92,0A5.14,5.14,0,0,0,247,53.72a5.27,5.27,0,0,0-1.27-3.66,4.94,4.94,0,0,0-6.94,0,5.24,5.24,0,0,0-1.28,3.64h0Zm13.81-.1a8.66,8.66,0,0,1-2.55,6.47,9.71,9.71,0,0,1-13,0,8.68,8.68,0,0,1-2.55-6.45,8.4,8.4,0,0,1,2.62-6.34,9.59,9.59,0,0,1,12.92,0A8.42,8.42,0,0,1,251.36,53.62Z" transform="translate(-197.66 -38.27)"></path></svg></a>
                <div class="c-header-text">Моментальные займы. <br/>Просто. Удобно. Быстро</div>
            </div>
            <div class="c-header-right">
                <div class="c-header-phone-wrap"><a class="c-header-phone-number" style="background-image: url(https://evodengi.ru/wp-content/themes/evo_new/img/partials/header/icons/ruble_take.png);" href="tel:88005511955">8 800 55-119-55</a>
                    <div class="c-header-phone-text">Бесплатно. Круглосуточно</div>
                    <div class="c-btn-toggle"><span></span><span></span><span></span></div>
                </div><a class="c-btn c-btn-type2 js-popup" href="/oformit-zajm/t">Получить деньги</a>
            </div>
            <style>
            @media only screen and (max-width: 544px) {
.c-header-right .c-btn-type2 {
    display: none;
}
            </style>
        </div>
        <div class="container container_style1">
            <nav class="c-header-nav">
                <?php
                wp_reset_query();
                wp_nav_menu( array(
                    'theme_location'  => 'top-menu',
                    'container'       => false,
                    'menu_class'      => 'c-unlist',

                ) );
                ?>

            </nav>
        </div>
    </header>

