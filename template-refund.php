<?php
/*
  * Template Name: refund
  */
?><?php
get_header();?>
<main class="c-main">
				<section class="c-block-type8 c-block-type8-padding">
					<div class="container">
						<div class="c-block-type8-top">
							<h2 class="c-main-title-type1">Погашение займа</h2>
						</div>
						<div class="c-block-type8-middle">
							<div class="c-block-type8-steps-nav" id="formWithStepsNav">
								<div class="c-block-type8-steps-nav-items">
									<div class="c-block-type8-steps-nav-item active">
										<div class="c-block-type8-steps-nav-item-title">Шаг 1</div>
										<div class="c-block-type8-steps-nav-item-descr">Поиск займа</div>
									</div>
									<div class="c-block-type8-steps-nav-item">
										<div class="c-block-type8-steps-nav-item-title">Шаг 2</div>
										<div class="c-block-type8-steps-nav-item-descr">Оплата</div>
									</div>
								</div>
							</div>
						</div>
						<div class="c-block-type8-bottom">
							<div class="c-block-type8-bottom-left">
								<div class="c-block-type8-form-wrap" id="formWithSteps">
									<form>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Поиск займа</div>
											<div class="c-block-type8-form-bottom-descr">Для получения информации по займу, введите номер телефона на который была оформлена заявка или номер договора из СМС</div>
											<div class="c-block-type8-form-item c-block-type8-form-item_style1">
												<div class="c-block-type8-form-item-left c-block-type8-form-item-left_style1">
													<input type="radio" name="refoundRadio" value="По номеру телефона" id="radio1" checked="checked"/>
													<label for="radio1">По номеру телефона</label>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="tel" name="phoneRefound" placeholder="+7 (900)-000-0000" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-item">
												<div class="c-block-type8-form-item-left c-block-type8-form-item-left_style1">
													<input type="radio" name="refoundRadio" value="По номеру договора" id="radio2"/>
													<label for="radio2">По номеру договора</label>
												</div>
												<div class="c-block-type8-form-item-right">
													<input type="text" name="contractNumber" placeholder="0000" value="" data-validate="true"/>
												</div>
											</div>
											<div class="c-block-type8-form-bottom">
												<button class="c-btn c-btn-type2 js-setContractNumber" type="button" data-step="#popup-code2" disabled>Далее</button>
											</div>
										</div>
										<div class="c-block-type8-form-step">
											<div class="c-block-type8-form-step-title">Выберите способ оплаты</div>
											<div class="c-block-type8-payment">
												<div class="c-block-type8-payment-item">
													<input type="radio" name="radio-name1" value="radio1-value" id="radio3" checked="checked"/>
													<label for="radio3">Банковской картой</label>
													<div class="c-block-type8-payment-item-img"><img src="img/elements/block8/images/pay1.png" alt="alt"></div>
												</div>
												<div class="c-block-type8-payment-item">
													<input type="radio" name="radio-name1" value="radio1-value" id="radio4"/>
													<label for="radio4">Электронный кошелек</label>
													<div class="c-block-type8-payment-item-img"><img src="img/elements/block8/images/pay2.png" alt="alt"></div>
												</div>
												<div class="c-block-type8-payment-item">
													<input type="radio" name="radio-name1" value="radio1-value" id="radio5"/>
													<label for="radio5">Сбербанк-онлайн</label>
													<div class="c-block-type8-payment-item-img"><img src="img/elements/block8/images/pay3.png" alt="alt"></div>
												</div>
											</div>
											<div class="c-block-type8-form-bottom">
												<button class="c-btn c-btn-type2" type="button" data-type="thanks">Далее</button>
											</div>
										</div>
										<div class="c-block-type8-form-step" id="info">
											<div class="c-block-type8-form-step-title">Информация по займу</div>
											<div class="c-block-type8-info">
												<div class="c-block-type8-info-items">
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">Номер договора</div>
														<div class="c-block-type8-info-item-text js-contractNumber">3245</div>
													</div>
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">Сумма займа</div>
														<div class="c-block-type8-info-item-text">23 000 руб.</div>
													</div>
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">ФИО</div>
														<div class="c-block-type8-info-item-text">Петрова Ирина Олеговна</div>
													</div>
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">Проценты (на сегодняшний день)</div>
														<div class="c-block-type8-info-item-text">2 343 руб.</div>
													</div>
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">Дата займа</div>
														<div class="c-block-type8-info-item-text">13 мая 2017</div>
													</div>
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">Общая сумма к погашению</div>
														<div class="c-block-type8-info-item-text">25 343 руб.</div>
													</div>
													<div class="c-block-type8-info-item">
														<div class="c-block-type8-info-item-title">Срок займа (на сегодняшний день)</div>
														<div class="c-block-type8-info-item-text">14 дней</div>
													</div>
												</div>
												<div class="c-block-type8-info-bottom">
													<div class="c-block-type8-info-bottom-title">Сумма к оплате</div>
													<div class="c-block-type8-info-bottom-text">Общая сумма к погашению составила <span>25 343 руб.</span></div>
													<button class="c-btn c-btn-type2" type="button" data-step="2">Погасить займ</button>
												</div>
											</div>
										</div>
										<div class="c-block-type8-form-step" id="thanks">
											<div class="c-block-type8-result">
												<div class="c-block-type8-result-title">Погашение займа по договору №<span class="js-contractNumber">-- </span> успешно. <br>Займ полностью погашен. Спасибо!</div>
												<div class="c-block-type8-result-text">Теперь вам доступны займы на более выгодных условиях, следите за нашей рассылкой специальных предложений.</div>
												<button class="c-btn c-btn-type2" type="button" data-step="subscribe">Получать рассылку</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>

<?php get_footer();?>
