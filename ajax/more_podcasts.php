<?php
header("Content-Type: text/html; charset=utf-8");
require_once '../../../../wp-load.php';
$args = unserialize( stripslashes( $_POST['query'] ) );
$args['paged'] = $_POST['page'] + 1; // следующая страница
$args['post_status'] = 'publish';


$podcasts=new WP_Query($args);
if($podcasts->have_posts()):
	$i=0;
	while ($podcasts->have_posts()):
		$podcasts->the_post();
		$i++;
		if($i%7==1)
			get_template_part('flexible/case_big');
		else
			get_template_part('flexible/case_normal');
	endwhile;
endif;
wp_reset_query();
