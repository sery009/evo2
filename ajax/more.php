<?php
header("Content-Type: text/html; charset=utf-8");
require_once '../../../../wp-load.php';
$args = unserialize( stripslashes( $_POST['query'] ) );
$args['paged'] = $_POST['page'] + 1; // следующая страница
$args['post_status'] = 'publish';

// обычно лучше использовать WP_Query, но не здесь
query_posts( $args );
$i=0;
//var_dump($args);
// если посты есть
if( have_posts() ) :
    while( have_posts() ): the_post();
        $i++;

            get_template_part("parts/article");

    endwhile;

endif;