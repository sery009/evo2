<?php get_header();?>
<?php wp_reset_query();
update_post_meta(get_the_ID(),"kub_views",(+get_post_meta(get_the_ID(),"kub_views",true)+1));
?>

    <main class="c-main">
        <section class="c-block-type6 c-block-type6-padding">
            <div class="container">
                <div class="c-block-type6-items"><a class="c-block-type6-item" href="<?php bloginfo('url')?>/news">Новости и статьи</a>
                    <div class="c-block-type6-item"><?php the_title();?></div>
                </div>
            </div>
        </section>
        <section class="c-block-type7 c-block-type7-padding">
            <div class="container">
                <div class="c-block-type7-left">
                    <div class="c-block-type7-content">
                        <div class="c-block-type7-content-top">
                            <div class="c-block-type7-content-top-items">
                                <div class="c-block-type7-content-date"><?php the_time('d F Y')?></div>
                                <div class="c-block-type7-content-views">
                                    <div class="c-ico c-ico-views"></div><span><?php echo get_post_meta(get_the_ID(),"kub_views",true);?></span>
                                </div>
                                <!--<div class="c-block-type7-content-comments">
                                    <div class="c-ico c-ico-comments"></div><span>12</span>
                                </div>-->
                            </div>
                        </div>
                        <div class="c-block-type7-content-bottom">
                            <h2 class="c-main-title-type1"><?php the_title();?></h2>
                            <div class="c-block-type7-content-text">
                                <?php echo thumb(get_the_ID(),"medium");?>
                                <?php the_content();?>
                            </div>
                            <div class="c-block-type7-social-share"><img src="<?php echo THEME_IMG;?>/elements/block7/images/social-panel.png"></div>
                        </div>
                    </div>
                </div>
                <div class="c-block-type7-right">
                    <?php get_template_part('parts/calc')?>
                </div>
            </div>
        </section>
    </main>
<?php get_footer();?>